FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " file://scale-wpa.conf"

do_install_append () {
  sed -i -e 's/WPA-SUPPLICANT_SSID/${WPA-SUPPLICANT_SSID}/g' ${WORKDIR}/scale-wpa.conf
  sed -i -e 's/WPA-SUPPLICANT_PSK/${WPA-SUPPLICANT_PSK}/g' ${WORKDIR}/scale-wpa.conf
  cat ${WORKDIR}/scale-wpa.conf > ${D}${sysconfdir}/wpa_supplicant.conf
}

