SUMMARY = "esp8089 module support"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8264535c0c4e9c6c335635c4026a8022"

inherit module

SRC_URI = "git://github.com/Icenowy/esp8089.git;branch=cleanup \
           file://0001-Makefile.patch"
SRCREV = "71c53ead1ce24fb5cee1f9c2eb251be1f3125e37"


#SRC_URI = "git://github.com/al177/esp8089.git"
#SRCREV = "58918e272f22da212045f6280c454085456ca1c0"

#MODULES_INSTALL_TARGET = 'install'
#EXTRA_OEMAKE += "'KBUILD=${STAGING_KERNEL_BUILDDIR}' 'DESTDIR=${D}'"



S = "${WORKDIR}/git"

do_install_append () {
    install -d ${D}/lib/firmware
    install -m 0644 ${B}/firmware/*.bin ${D}/lib/firmware/
}

FILES_${PN} += "/lib/firmware"

